var http = require('http');
var url = require('url');
var fs = require('fs');
var cookie = require('cookie');
var querystring = require('querystring');
//连接数据库
var mysql      = require('mysql');
//创建连接，使数据库和后台连接起来
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'ajaxdemo'
});
//连接数据库
connection.connect();

var app = http.createServer(function (req, res) {
    var url_obj = url.parse(req.url);
    // if(url_obj.pathname === '/'){
    //     reader('./template/home.html', res);
    // }
    // if(url_obj.pathname === '/login'){
    //     reader('./template/login.html', res);
    // }
    // if(url_obj.pathname === '/css/home.css'){
    //     reader('./template/css/home.css', res);
    // }
    // console.log(url_obj.pathname );


    //实现网页文件的读写
    if(url_obj.pathname === '/'){
        reader('./template/index.html', res);
        //返回结束，直接结束代码执行
        return;
    }
    //注册模块实现
    if(url_obj.pathname === '/register' && req.method === 'POST'){
        res.setHeader('content-type','text/html; charset=utf-8');
        var user_info = '';
        req.on('data', function (chunk) {
            user_info += chunk;
        });
        req.on('end', function (err) {
            if(!err){
                var user_obj = querystring.parse(user_info);
                if(user_obj.username === '' || user_obj.password === '' || user_obj.repassword === '' ){
                    res.write('{"status":1, "message":"用户名或密码不能为空"}');
                    res.end();
                    return;
                }
                if(user_obj.password !== user_obj.repassword ){
                    res.write('{"status":1, "message":"两次输入密码不一致！！"}');
                    res.end();
                    return;
                }
                //后台发送命令到数据库，对数据库执行操作
                var sql = "INSERT INTO admin(username, password) VALUE ('" + user_obj.username + "', '"+ user_obj.password + "')";
                connection.query(sql, function (error, results, fields) {
                    res.write('{"status":0, "message":"注册成功！！"}');
                    res.end();
                    return;
                });
            }

        })

    }
    //登录模块实现
    if(url_obj.pathname === '/login' && req.method === 'POST'){
        var user_info = '';
        req.on('data', function (chunk) {
            user_info += chunk;
        });
        req.on('end', function (err) {
            // console.log(user_info);
            if(!err){
                res.setHeader('content-type', 'text/html;charset=utf-8');
                var user_obj = querystring.parse(user_info);
                var sql = "SELECT * FROM admin WHERE username= '" + user_obj.username + "'AND password='" + user_obj.password + "'";
                connection.query(sql, function (error, result) {
                    if(!error && result && result.length !== 0){
                        //登陆成功，发送cookie
                        res.setHeader('Set-Cookie', cookie.serialize('isLogin', 'true'));
                        res.write('{"status": 0, "message": "后台数据匹配完成,登陆成功！！"}','utf-8');
                        res.end();
                    }else{
                        res.write('{"status": 1, "message": "账户名或密码错误！！"}','utf-8');
                        res.end();
                    }
                });

            }
        });
        return;
    }
    //渲染表格到前端
    if(url_obj.pathname === '/list' && req.method === 'GET'){
        var sql = 'SELECT * FROM user';
        connection.query(sql, function (error, result) {
            res.setHeader('content-type', 'text/html;charset=utf-8');
            // console.log(result);
            if(!error && result){
                var arrstr = JSON.stringify(result);
                // console.log(arrstr);
                res.write('{"status": 0, "data": ' + arrstr + '}');
                res.end();
            }
        });
        return;
    }
    //添加用户信息
    if(url_obj.pathname === '/adduser' && req.method === 'POST'){
        var user_info = '';
        req.on('data', function (chunk) {
            user_info += chunk;
        });
        req.on('end', function (err) {
            res.setHeader('content-type', 'text/html;charset=utf-8');
            if(!err){
                var user_obj = querystring.parse(user_info);
                var sql = 'INSERT INTO user VALUE (' + null + ',"' + user_obj.username + '","'
                    + user_obj.email + '","' + user_obj.cellphone + '","' + user_obj.QQ +'")';
                connection.query(sql, function (error, result) {
                    if(!error && result && result.length !== 0){
                        res.write('{"status":0, "message":"用户信息更改成功！！"}');
                        res.end();
                    }else{
                        res.write('{"status":1, "message":"数据提交错误！！"}');
                        res.end();
                    }
                });
            }
        });
        return;
    }
    //编辑用户信息并保存到数据库
    if(url_obj.pathname === '/updata' && req.method === 'POST'){
        res.setHeader('content-type', 'text/html;charset=utf-8');
        var user_info = '';
        req.on('data', function (chunk) {
            user_info += chunk;
        });
        req.on('end', function (err) {
            if(!err){
                var user_obj = querystring.parse(user_info);
                var sql = 'UPDATE user SET username="' + user_obj.username + '",email="' + user_obj.email +
                    '",cellphone="' + user_obj.cellphone + '",QQ="' + user_obj.QQ + '" WHERE id=' + Number(user_obj.id);
                connection.query(sql, function (error, result) {
                    if(!error && result && result.length !== 0){
                        res.write('{"status":0, "message":"用户信息更新成功！！"}');
                        res.end();
                    }else{
                        res.write('{"status":1, "message":"数据更新错误！！"}');
                        res.end();
                    }

                })
            }
        });
        return;
    }
    //删除用户信息
    if(url_obj.pathname === '/delete' && req.method === 'POST'){
        var user_info = '';
        req.on('data', function (chunk) {
            user_info += chunk;
        });
        req.on('end', function (err) {
            if(!err){
                var user_obj = querystring.parse(user_info);
                var sql = 'DELETE FROM user WHERE id=' + Number(user_obj.id);
                connection.query(sql, function (error, result) {
                    if(!error && result && result.length !== 0){
                        res.write('{"status":0, "message":"用户信息删除成功！！"}');
                        res.end();
                    }else{
                        res.write('{"status":1, "message":"数据删除错误！！"}');
                        res.end();
                    }
                })
            }
        });
        return;
    }

    //从后台发送静态文件到浏览器进行渲染
        //通过cookie判断客户端是否登陆过
    if(url_obj.pathname === '/admin.html'){
        var cookie_obj = cookie.parse(req.headers.cookie || '');
        if(cookie_obj.isLogin === 'true'){
            reader('./template/admin.html', res);
        }else{
            reader('./template/error.html', res);
        }
        return;
    }
    //点击退出登录
    if (url_obj.pathname === '/logout'){
        res.setHeader('Set-Cookie', cookie.serialize('isLogin', ''));
        reader('./template/index.html', res);
        res.end();
        return;
    }


    //读写文件内容
    reader('./template' + url_obj.pathname, res);

    //封装读写内容函数
    function reader(path, res) {//  binary:二进制编码
        fs.readFile(path, 'binary', function (err, data) {
            if(!err){
                res.write(data, 'binary');
                res.end();
            }
        })
    }
});
app.listen(4000);













